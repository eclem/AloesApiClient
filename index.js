const EventEmitter = require('events');
const request = require('request');
const WebSocket = require('ws');

class AloesApiClient extends EventEmitter {

    constructor(args) {

        super();

        this.host = args.host || 'localhost';
        this.port = args.port || '3000';
        this.username = args.username || 'user';
        this.password = args.password || 'password';
        this.secure = args.secure || false;
        this.debug = args.debug || false;
        this.wsAttemptsMax = args.debug || 10;

        this.ws = {};
        this.onmessage;
    }

    isJson(str) {

        try { JSON.parse(str) }
        catch (e) { return false }
        return true;
    }

    connect(callback) {

        var nbHttp = 1;

        const makeHttpReq = (host, port, form) => {

            this.debug && console.log('DEBUG sending http req', nbHttp)

            request.post({'url': (this.secure ? 'https' : 'http') + '://' + host + ':' + port + '/api/login', 'form': form}, (err, httpResponse, body) => {

                if (err) {

                    this.debug && console.log('DEBUG error: ', err.code);

                    nbHttp++;

                    setTimeout( () => {

                        makeHttpReq(host, port, form);

                    }, 1000);

                } else {

                    this.debug && console.log('DEBUG http req ok')

                    nbHttp = 1;

                    var nbWs = 1;

                    const makeWsReq = (host, port, cookie) => {

                        this.debug && console.log('DEBUG opening ws', nbWs);

                        this.ws = new WebSocket((this.secure ? 'wss' : 'ws') + '://' + host +':' + port, { 'headers': { 'Cookie': cookie }});

                        this.ws.onopen = () => {

                            this.debug && console.log('DEBUG ws opened');

                            nbWs = 0;

                            this.emit('connected');
                        };

                        this.ws.onmessage = message => {

                            if (this.isJson(message.data)) {

                                message = JSON.parse(message.data);

                                this.emit('message', message);
                            }
                        };

                        this.ws.onerror = err => {

                            this.debug && console.log('DEBUG error: ', err.code);
                        };

                        this.ws.onclose = () => {

                            this.debug && console.log('DEBUG ws closed');

                            nbWs++;

                            this.emit('disconnected');

                            setTimeout( () => {

                                (nbWs <= this.wsAttemptsMax) ? makeWsReq(host, port, cookie) : makeHttpReq(host, port, form);

                            }, 1000);
                        };
                    }

                    makeWsReq(host, port, httpResponse.headers['set-cookie'][0]);
                }
            });
        };

        makeHttpReq(this.host, this.port, {'login': this.username, 'password': this.password});
    }

    send(sendMessage, callback) {

        if (this.ws.readyState === 1) {

            this.ws.send( JSON.stringify( sendMessage ));

            this.ws.onmessage = message => {

                if (this.isJson(message.data)) {

                    message = JSON.parse(message.data);

                    this.emit('message', message);

                    if ((message.content === 'res') && (message.answerTo === sendMessage.action)) {

                        this.ws.onmessage = message => {

                            this.emit('message', message);
                        };

                        callback(null, message.data);
                    }
                }
            };

        } else {

            callback('not sent: not connected');
        }
    }
}

module.exports = AloesApiClient;