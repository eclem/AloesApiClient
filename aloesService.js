const EventEmitter = require('events');
const AloesApiClient = require('./index');

class AloesService extends EventEmitter {

    constructor() {

        super();

        this.client = new AloesApiClient({
            host: 'localhost',
            port: '3000',
            username: 'user',
            password: 'password',
            secure: false
        });

        this.client.connect();

        this.client.on('connected', () => {

            this.emit('connected');
            console.log('connected');
        });

        this.client.on('message', message => {

            switch (message.content) {

                case 'deviceState':
                    console.log('deviceState')
                    break;

                case 'user':
                    console.log('user')
                    break;

                case 'points':
                    console.log('points')
                    break;

                default:
                    console.log('unknown message :', message);
            };
        });

        this.client.on('disconnected', () => {

            console.log('disconnected');
        });
    }

    send(message, callback) {

        this.client.send(message, callback);
    }
}

module.exports = new AloesService();