# Installation

```
npm install git+https://framagit.org/eclem/AloesApiClient.git
```
# Usage

```
const AloesApiClient = require('AloesApiClient');

const client = new AloesApiClient({
    host: 'localhost',
    port: '3000',
    username: 'user',
    password: 'password',
    secure: false
});

client.connect();

client.on('connected', () => {

    client.send({'action': 'ping'}, (err, res) => {

        if (err) {

            console.log(err)
        }

        console.log('response:', res);
    });
});

client.on('message', message => {

    console.log('incoming message:', message)
});
```